# Hemma

**This project is discontinued**

*DarkSky sold themselves to Apple, who then discontinued their API.*

*This project relies on DarkSky natural-language weather descriptions for its aesthetics, and makes little sense without it.*

![Screenshot of the app](screenshot.png)

A page to display time, weather and buses from a particular
stop in the Västra Götaland region, in Sweden.

This page is meant to run completely on the browser, either
from the local file system, or served from a static host. 
Tested on Firefox 55 and Firefox for Android 56.

For this application to retrieve data from the DarkSky and Västtrafik API, you may need to selectively disable CORS:

*Inform yourself of the technical, security and legal implications of all the steps below
before taking them.*

1. Install [Corser](https://addons.mozilla.org/en-US/firefox/addon/corser/).
2. Go to the add-on's preferences and add this to the rules field:

   ```
   file:///*
   -
   *://*.darksky.net/*
   *://api.vasttrafik.se/*
   ```
3. Press “Save”.

If the app is served from a static host, replace `file:///*` with the glob
pattern corresponding to your host.

## Repository contents

+ `config.js`: Configuration. 

+ `secret.js.sample`: Secret API keys. Rename to `secret.js`.
   You will need to obtain both [DarkSky](https://darksky.net/dev)
   and [Västtrafik](https://developer.vasttrafik.se/portal/) API keys.  
   Note that, by design, the API keys are exposed to the client browser.
   *Inform yourself of the technical, security and legal implications of this fact, specially if you will be hosting the application files in a location which is potentially accessible by others.*

+ `home.html`: Main application file. Open in the browser.
+ `home.js`: Application logic. Interaction with API.
+ `home.css`: Stylesheet. Feel free to modify it to your liking.
+ `local.css.sample`: Rename to `local.css`. 

## Licenses

   + `jquery-3.2.1.js`: MIT, see [website](http://jquery.com/).
   + `jquery-dateFormat.js`: MIT, see [repo](https://github.com/phstc/jquery-dateFormat).
   + `jquery.sparkline.js`: New BSD, see [website](https://omnipotent.net/jquery.sparkline/).
   + Rest of the files: [GPLv3](LICENSE) or any later version.
   
## Notes

Concept and bus-arrival-time color coding inspired by the `panel` component of LtWorf's
[siddio](https://github.com/ltworf/siddio) project.
   

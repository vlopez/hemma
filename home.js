
function update_weather() {
  var url = "https://api.darksky.net/";
  var key = WEATHER_KEY;
  var latlon = WEATHER_LAT_LON;

  $.ajax(url + "forecast/" + key + "/" + latlon,
        {"data":{"lang":"sv","units":"si"},"cache":false}).done(
	  function(data) {
		  $("[name='weather-summary']").html(data.hourly.summary);
		  var temp_min = 1000; var temp_max = -1000; var wind_max = 0;
		  temp = [];
                  now  = Date.now()/1000;
                  j = 0;
		  for(i=0;i<data.hourly.data.length;i++){
			  hour = data.hourly.data[i];
			  if(now<=hour.time && hour.time<=now+WEATHER_WIND_HOURS*3600){
			    temp[j] = hour.temperature;
                            j++;
			    temp_min = Math.min(temp_min, hour.temperature);
                            temp_max = Math.max(temp_max, hour.temperature);
                          }
			  if(now<=hour.time && hour.time<=now+WEATHER_WIND_HOURS*3600){
			    wind_max = Math.max(wind_max, hour.windSpeed);
			  }
		  }
		  $("[name='weather-info']").html(
			  pretty_number(temp_min) + ' <a class="temperature-spark">···</a> '
			+ pretty_number(temp_max) + "°C" + " ~ "
			+ pretty_number(wind_max) + 
                          " ㎧"
		  );
		  $(".temperature-spark").sparkline(temp, 
			{"width":"2em",
                         "height":"1em",
			 "fillColor":false,
		         "lineColor":$(".temperature-spark").css("color"),
			 "lineWidth":5,
                         "spotColor":false,
                         "maxSpotColor":false,
                         "minSpotColor":false});
		  $("[name='weather-summary-2']").html(data.daily.summary);

		  setBackdrop(data);
	  });
}



function pretty_number(x) {
  return Math.round(x).toString().replace("-","−");
}

function svensk_klocka(date) {
	var time =	$.format.date(date,'h:mm')
	var mid  =	$.format.date(date,'p')
	if (mid[0] == 'p') { return time + ' <a class="pm">e.m.</a>' }
	else if (mid[0] == 'a') { return time + ' <a class="pm">a.m.</a>' }
}

function update_clock() {
	$('[name=clock]').html(svensk_klocka(new Date()));
}


var bus_departures = false;

function redraw_buses() {
	var i_track = BUS_TRACK;
	var max_buses = MAX_BUSES;
	if (!bus_departures) { return; }
        edep = $("[name='bus-departures']");
	edep.html("")
	for(i=0,count=0;count<max_buses;i++) {
		depTime = prettyVasttrafikDeparture(
			bus_departures[i].rtDate || bus_departures[i].date,
			bus_departures[i].rtTime || bus_departures[i].time);

		if(bus_departures[i].track == i_track){
			count++;
			edep.append('<li class="' + depTime.urgency + '">' +
				  depTime.time + " ··· " +
				  "<strong>" + bus_departures[i].name + "</strong>" + "<a class=\"destination\"> mot " +
				  bus_departures[i].direction +
				   "</a></li>");	
		}
	}
}

var bus_access_token = false;
function update_access_token(f) {
	// Get access token
	var secret = VASTTRAFIK_KEY;
	var url0 = "https://api.vasttrafik.se/token";
	$.post({"url":url0,"headers":{"Authorization": "Basic " + secret},"data":{"grant_type":"client_credentials"}}).done(function (data) {
	bus_access_token = data.access_token;
	// Will try to renovate the token 4 times before it expires 
	f();
	})
}

function update_buses() {
	var i_stopname = BUS_STOP_NAME; 
	var date = new Date() 
	var i_date = $.format.date(date,'yyyy-MM-dd'); 
	var i_time = $.format.date(date,'HH:mm'); 

	if (!bus_access_token) { return; }
	var key = bus_access_token;
	// Get the stop id (first stop in the result list)
	var url1 = "https://api.vasttrafik.se/bin/rest.exe/v2/location.name?format=json";
	$.ajax(url1, {"headers":{"Authorization":"Bearer " + key}
	             ,"data":{"input":i_stopname}}).done(function(data){
	var stop = data.LocationList.StopLocation[0];

	// Get departures from the stop
	var url2 = "https://api.vasttrafik.se/bin/rest.exe/v2/departureBoard?format=json";
	$.ajax(url2, {"headers":{"Authorization":"Bearer " + key}
	             ,"data":{"id":stop.id,"date":i_date,"time":i_time}}).done(function(data) {

	bus_departures = data.DepartureBoard.Departure;
    bus_departures.sort(function (a,b) { 
        if (a.rtDate < b.rtDate) { return -1; }
        if (a.rtDate > b.rtDate) { return  1; }
        if (a.rtTime < b.rtTime) { return -1; }
        if (a.rtTime > b.rtTime) { return  1; }
        if (a.name   < b.name  ) { return -1; }
        if (a.name   > b.name  ) { return  1; }
        return 0;
	})})})
  
}

function prettyVasttrafikDeparture(day, time) {
	// Assume system timezone is the same as server timezone
	var now  = new Date();
	var date = new Date(day + " " + time);
	var minutes = (date - now)/(60 * 1000);
	if (minutes <= 0) { time = "av" }
	else if (minutes < 1) { time = "nu" }
	else if (minutes < 20) { time = Math.floor(minutes) + " min" }
	else { time = svensk_klocka(date) }

	if (minutes < 3) { urgency = "gone" }
	else if (minutes < 6) { urgency = "hurry" } 
	else if (minutes < 9) { urgency = "ok" }
	else if (minutes < 15) { urgency = "soon" }
	else { urgency = "wait" }

	return {"time":time
	       ,"urgency":urgency
	       }
}

function setBackdrop(data) {
	var kodi_path = "art/resource.images.weatherfanart.multi/resources/";
	var backdrops = [
           {"file":kodi_path+"na/weather-NA-00.jpg"}
          ,{"file":kodi_path+"32/weather-32-10.jpg",conditions:["clear-day"]}
          ,{"file":kodi_path+"31/weather-31-08.jpg",conditions:["clear-night"]}
          ,{"file":kodi_path+"9/weather-9-07.jpg"  ,conditions:["rain"]}
          ,{"file":kodi_path+"15/weather-15-08.jpg",conditions:["snow"]}
          ,{"file":kodi_path+"18/weather-18-09.jpg",conditions:["sleet"]}
          ,{"file":kodi_path+"24/weather-24-06.jpg",conditions:["wind"]}
          ,{"file":kodi_path+"20/weather-20-00.jpg",conditions:["fog"]}
          ,{"file":kodi_path+"26/weather-26-07.jpg",conditions:["cloudy"]}
          ,{"file":kodi_path+"44/weather-44-27.jpg",conditions:["partly-cloudy-day"]}
          ,{"file":kodi_path+"27/weather-27-06.jpg",conditions:["partly-cloudy-night"]}
          ];
 
	var sel = null;
	// Choose the best image
	$.each(backdrops, function (index, img) {
		var can = {"prio":($.inArray(data.hourly.icon,img.conditions)>=0)?1:0
		          ,"url" :img.file
		          }
		if (sel == null || sel.prio < can.prio) { sel = can };
	})
	var url = sel.url;
	$("body").css("background-image","url("+url+")");
}
		

function every(minutes,f) {
	f(); setInterval(f, minutes*60*1000);
}

$(function(){
	update_access_token(function () {
        every(10, function () { update_access_token(function () {})});
	every(1,update_buses);
	every(5/60,redraw_buses);
	every(10,update_weather);
	every(0.5/60,update_clock);
	})
});



